
/**
 * Module dependencies.
 */

var path = require('path');
var extend = require('util')._extend;

var development = require('./env/development');
var test = require('./env/test');
var production = require('./env/production');

var defaults = {
  root: path.normalize(__dirname + '/..')
};
console.log('Loading config module');
console.log('NODE_ENV:', process.env.NODE_ENV);
console.log('module.exports', extend(development, defaults));
/**
 * Expose
 */

// module.exports = {
//   development: extend(development, defaults),
//   test: extend(test, defaults),
//   production: extend(production, defaults)
// }[process.env.NODE_ENV || 'development'];
module.exports = development;
